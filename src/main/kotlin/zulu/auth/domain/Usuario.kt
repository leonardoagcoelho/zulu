package zulu.auth.domain

data class Usuario(val id : String,
                   val nome : String,
                   val password : String,
                   val funcao : Funcao)
