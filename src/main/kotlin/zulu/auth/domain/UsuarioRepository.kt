package zulu.auth.domain

interface UsuarioRepository {
    fun getAll() : List<Usuario>
    fun getById(id : String) : Usuario?
    fun upsert(usuario : Usuario)
}