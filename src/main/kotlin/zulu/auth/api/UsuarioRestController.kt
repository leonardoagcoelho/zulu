package zulu.auth.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import zulu.auth.domain.Usuario
import zulu.auth.domain.UsuarioRepository

@RestController
@RequestMapping("/api/v2")
class UsuarioRestController @Autowired constructor(
        private val usuarioRepository: UsuarioRepository

) {

    @GetMapping("/usuarios")
    fun getAll(): List<Usuario> {
        return usuarioRepository.getAll()
    }

    @GetMapping("/usuarios/{id}")
    fun getById(@PathVariable("id") id : String): Usuario {
        return usuarioRepository.getById(id) ?: throw UsuarioNaoEncontradoException()
    }
}

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Usuário não encontrado.")
class UsuarioNaoEncontradoException : Exception()