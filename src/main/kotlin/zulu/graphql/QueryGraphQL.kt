package zulu.graphql

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import org.springframework.stereotype.Component

@Component
class QueryGraphQL: GraphQLQueryResolver{
    fun hello(): String {
        return "Olar mundo"
    }
}