package zulu

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ZuluApplication

fun main(args: Array<String>) {
	runApplication<ZuluApplication>(*args)
}
