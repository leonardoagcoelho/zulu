package zulu.operacoes.domain

interface OperacaoRepository {
    fun getAll() : List<Operacao>
    fun getById(id : String) : Operacao?
    fun upsert(operacao : Operacao)
}